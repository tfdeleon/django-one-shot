from django.urls import path
from todos.views import (
    todo_list,
    todo_item,
    create_todo_list,
    todo_list_update,
    todo_list_delete,
)

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_item, name="todo_list_detail"),
    path("create/", create_todo_list, name="todo_list_create"),
    path("<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
]
