from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.


def todo_list(request):
    todo = TodoList.objects.all()
    context = {
        "todolist": todo,
    }
    return render(request, "todos/list.html", context)


def todo_item(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_item": detail,
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", id=detail.id)
    else:
        form = TodoListForm()
    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id = list.id)
    else:
        form = TodoListForm(instance=list)
    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)

def todo_list_delete(request, id):
    if request.method == "POST":
        list = TodoList.objects.get(id=id)
        list.delete()
        return redirect("todo_list")
    return render(request, "todos/delete.html")
